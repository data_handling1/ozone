import numpy as np
import xarray as xr


def calc_time_mean(ds):
    """Calculates the time mean of data.

    Args:

       ds (xr.Dataset)

    Returns: dataset with mean over time

    """

    ds = ds.mean(dim="time",keep_attrs=True)

    return ds


def calc_seasonal_mean(ds,weighted=None):
    """Calculates the seasonal means of timeseries.

    Args:

       ds (xr.Dataset) containing dimension time

       weighted (boolean): if true use weighted mean

    Returns: seasonal mean of dataset

    """

    if weighted:
        # weight season by number of days of months
        month_len = ds.time.dt.days_in_month
        weights = month_len.groupby("time.season") / month_len.groupby("time.season").sum(keep_attrs=True)
        ds = (ds * weights).groupby("time.season").sum(dim="time")
    else:
        ds = ds.groupby("time.season").mean(dim="time")

    return ds


def calc_var_altitude(ds,var="ozone"):
    """Calculates altitude where variable has maximum.

    Args:

       ds (xr.Dataset): dataset containing chosen variable

       var (str): variable name. default: ozone

    Returns: xr.DataArray with altitude of var maximum.

    """

    var_max =  ds[var].max(dim="altitude")

    # get altitude for variable maximum
    alt_var_max = xr.where(ds.ozone==var_max,ds.altitude,np.nan)

    alt_var_max  = alt_var_max.mean(dim="altitude")
    alt_var_max = alt_var_max.rename(f"alt_{var}_max")

    alt_var_max.attrs["long_name"] = f"Altitude of {var} maximum "
    alt_var_max.attrs["units"] = "m"

    return alt_var_max


def calc_vertical_profile(ds,bins=[-85,-65,-15,15,65,85]):
    """Calculates the mean of a variable over latitude range.

    Args:

       ds (xr.Dataset): containing variable and latitudes

       bins (list,default:[-90,-60,-30,30,60,90])

    Returns: da with mean vertical profile

    """
    # works bins=[-90,-60,-30,30,60,90]
    bin_labels = ["south polar","south mid","tropics","north mid","north polar"]

    ds = ds.mean(dim="longitude_bins",keep_attrs=True)
    ds = ds.groupby_bins("latitude_bins",bins,labels=bin_labels).mean(keep_attrs=True)
    ds = ds.rename({'latitude_bins_bins':'latitude_bins'})
    ds =ds.dropna(dim="altitude")

    return ds


def calc_var_layer(ds,lower_bound=15000,upper_bound=50000):
    """Calculates the mean of variables within layer.

    Args:

       ds (xr.Dataset): containing altitude variable

       lower_bound (int, default: 150000): lower altitude boundary

       upper_bound (int, default: 500000): upper altitude boundary

    Returns xr.Dataset with variables mean within layer.

    """

    ds = ds.sel(altitude=slice(lower_bound,upper_bound))
    ds = ds.mean(dim="altitude",keep_attrs=True)

    return ds


def seperate_time(ds):
    """Splits time into years and months.

    Args:

       ds (xr.Dataset): containing time variable.

    Return xr.Dataset with year and month dimension

    """

    year = ds.time.dt.year
    month = ds.time.dt.month
    ds = ds.assign_coords(year=("time", year.data), month=(
        "time", month.data))
    ds = ds.set_index(time=("year", "month")).unstack("time")

    return ds


def calc_seasonal_timeseries(ds,first_month=6,last_month=8,lower_lat=-85,upper_lat=-65):
    """Calculates the mean of the desired time period and desired latitude extent.

    Args:

       ds (xr.Dataet). containing month dim and latitude

       first_month (int, default:6): first month which is included in mean

       last_month (int, default:8): last month which is included in mean

       lower_lat (int, default:-85): lower latitude which is included in mean

       upper_lat (int, default:-65): upper latitude which is included in mean

    Returns xr.Dataset with a seasonal timeseries

    """

    ds = ds.mean(dim="longitude_bins",keep_attrs=True)
    if lower_lat > upper_lat:
        print("lower_lat has to be smaller than upper lat")

    ds = ds.sel(latitude_bins=slice(lower_lat,upper_lat)).mean(
        dim="latitude_bins",keep_attrs=True)
    ds = ds.sel(month=slice(first_month,last_month)).mean(
        dim="month",keep_attrs=True)

    return ds
