import xarray as xr

def read_nc(fn):
    """Reads netCDF file"""

    ds = xr.open_dataset(fn)

    return ds
