import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as mpath
import cartopy.crs as ccrs

def plot_alt_var_max(da,output_dir=""):
    """Plots global map of variable distribution.

    Args:

    da (xr.DataArray): Contains variable on lat lon grid

    """

    ax = plt.axes(projection=ccrs.Robinson())
    ax.coastlines()
    ax.gridlines()
    da.T.plot(ax=ax, transform=ccrs.PlateCarree(),
               cbar_kwargs={'shrink': 0.5})
    plt.title(f"Global {da.long_name}")
    plt.tight_layout()

    if output_dir:
        plt.savefig(f"{output_dir}/global_dirstribution_{da.name}.png",dpi=300)
        plt.close()
        print(f"Plot of global distribution is saved to {output_dir}")

    else:
        plt.show()


def plot_seasonal_alt_var_max(da,output_dir=""):
    """Plots seasonal altitude of variable maximum.

    Args:

       da (xr.DataArray): array of of seasonal data to plot

       output_dir (str): path to where to save plot

    """

    fig, axes = plt.subplots(ncols=2,nrows=2, subplot_kw={'projection': ccrs.Robinson
                                                          ()}, figsize = (12, 8))
    axes=axes.flatten()
    for plot_nr in range(0,len(da.season)):
        ax = axes[plot_nr]
        ax.coastlines()
        ax.gridlines(draw_labels=False)
        da.isel(season=plot_nr).T.plot(ax=ax,
                                       transform=ccrs.PlateCarree(),
                                       vmin=30000,
                                       vmax=38000,
                                       cbar_kwargs={'shrink': 0.5})

    fig.suptitle(f"Seasonal {da.long_name}",fontsize=20)
    plt.tight_layout()

    if output_dir:
        plt.savefig(f"{output_dir}/seasonal_{da.name}.png",dpi=300)
        plt.close()
        print(f"Plot of seasonal {da.name} is saved to {output_dir}")

    else:
        plt.show()


def plot_vertical_profile(ds,var,altitude_var="altitude",output_dir=""):
    """Plots the mean profile of variable concentration.

    Args:

       ds (xr.Dataset): containing variable and altitude

       var (str): of which mean concentration depending on alt is calculated

       altitude_variable (str, default=altitude): altitude variable.

    """

    for bins in range(len(ds.latitude_bins)):
        plot_var = ds.isel(latitude_bins=bins)[var]
        alt_var = ds.isel(latitude_bins=bins)[altitude_var]
        label = ds.isel(latitude_bins=bins).latitude_bins.item(0)
        plt.plot(plot_var,alt_var,label=label)
        plt.xlabel(f"{ds[var].name} ({ds[var].units})")
        plt.ylabel(f"{ds[altitude_var].name} ({ds[altitude_var].units})")
        plt.grid()
        plt.legend()
        plt.title(f"Vertical profiles of mean {var} concentration")

    if output_dir:
        plt.savefig(f"{output_dir}/vertical_mean_profiles.png",dpi=300)
        plt.close()
        print(f"Plot of vertical mean profiles is saved to {output_dir}")

    else:
        plt.show()


def plot_var_layer(ds,var,output_dir=""):
    """Plot variable in certain altitude layer.

    Args:

       ds (xr.Dataset): dataset containing variable and altitude

       var (str): variable which is analyzed in altitude layer

       output_dir (str): path to output directory

    """

    theta = np.linspace(0, 2*np.pi, 100)
    center, radius = [0.5, 0.5], 0.5
    verts = np.vstack([np.sin(theta), np.cos(theta)]).T
    circle = mpath.Path(verts * radius + center)

    fig, axes = plt.subplots(ncols=2,nrows=2, subplot_kw={'projection': ccrs.SouthPolarStereo()}, figsize = (12, 8))
    axes=axes.flatten()

    for plot_nr in range(0,len(ds.season)):
        ax = axes[plot_nr]
        ax.coastlines()
        ax.set_boundary(circle, transform=ax.transAxes)
        ax.set_extent([-280, 80, -80, -35], crs=ccrs.PlateCarree())
        ax.gridlines(draw_labels=False,linestyle="--")
        ds.ozone.isel(season=plot_nr).T.plot(ax=ax,
                                             transform=ccrs.PlateCarree(),
                                             vmin=0.000004,
                                             vmax=0.000007,
                                             cbar_kwargs={'shrink': 0.5})

    fig.suptitle(f"Seasonal {var} layer",fontsize=20)
    plt.tight_layout()

    if output_dir:
        plt.savefig(f"{output_dir}/{var}_layer.png",dpi=300)
        plt.close()
        print(f"Plot of {var} layer saved to {output_dir}")

    else:
        plt.show()

def plot_seasonal_timeseries(ds,var,time_var="year",output_dir=""):

    plt.plot(ds[time_var],ds[var],label=ds[var].name)
    plt.grid()
    plt.legend()
    plt.xlabel(ds[time_var].name)
    plt.ylabel(f"{ds[var].name} ({ds[var].units})")
    plt.title(f"{ds[var].name} in stratosphere in south polar region (JJA)")

    if output_dir:
        plt.savefig(f"{output_dir}/{var}_seasonal_timeseries.png",dpi=300)
        plt.close()
        print(f"Plot of {var} seasonal timeseries saved to {output_dir}")

    else:
        plt.show()
