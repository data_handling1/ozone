import argparse
from pathlib import Path
from ozone import io_nc, calc, plot


def ozone():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-netcdf-file",
        dest="netcdf",
        required=True,
        help="Filename of input data in nc format.",
    )
    parser.add_argument(
        "-alt-var-max-plot",
        dest="alt_var_max_plot",
        action="store_true",
        help="Plot altitude of variable maximum."
    )
    parser.add_argument(
        "-seasonal-plot",
        dest="seasonal_plot",
        action="store_true",
        help="Plot seasonal altitude of ozone maximum."
    )
    parser.add_argument(
        "-vert-prof-plot",
        dest="vert_prof_plot",
        action="store_true",
        help="Plot vertical mean profile of variable."
    )
    parser.add_argument(
        "-var-layer-plot",
        dest="var_layer_plot",
        action="store_true",
        help="Plot mean variable within altitude layer."
    )
    parser.add_argument(
        "-seasonal-timeseries-plot",
        dest="seasonal_timeseries_plot",
        action="store_true",
        help="Plot seasonal timeseries (default:JJA)."
    )
    parser.add_argument(
        "-plot-var",
        dest="plot_var",
        type=str,
        required=True,
        help="Variable to plot."
    )
    parser.add_argument(
        "-out-dir",
        dest="out_dir",
        type=str,
        help="Output directory to store plot.",
    )

    args = parser.parse_args()

    if args.netcdf and not Path(args.netcdf).exists():
            parser.error(
                f"File {args.netcdf} does not exist.")

    if args.plot_var and hasattr(io_nc.read_nc(args.netcdf),args.plot_var) is False:
        parser.error(f"{args.plot_var} not a variable of the dataset.")

    if args.alt_var_max_plot:
        data = io_nc.read_nc(args.netcdf)
        data = calc.calc_time_mean(data)
        data = calc.calc_var_altitude(data,var=args.plot_var)

        plot.plot_alt_var_max(data,
                              output_dir=args.out_dir)

    if args.seasonal_plot:
        data = io_nc.read_nc(args.netcdf)
        data = calc.calc_var_altitude(data,var=args.plot_var)
        data = calc.calc_seasonal_mean(data)

        plot.plot_seasonal_alt_var_max(data,
                                       output_dir=args.out_dir)

    if args.vert_prof_plot:
        data = io_nc.read_nc(args.netcdf)
        data = calc.calc_time_mean(data)
        data = calc.calc_vertical_profile(data)

        plot.plot_vertical_profile(ds=data,
                                   var=args.plot_var,
                                   output_dir=args.out_dir)

    if args.var_layer_plot:
        data = io_nc.read_nc(args.netcdf)
        data = calc.calc_var_layer(data)
        data = calc.calc_seasonal_mean(data)

        plot.plot_var_layer(data,args.plot_var,
                            output_dir=args.out_dir)

    if args.seasonal_timeseries_plot:
        data = io_nc.read_nc(args.netcdf)
        data = calc.calc_var_layer(data)
        data = calc.seperate_time(data)
        data = calc.calc_seasonal_timeseries(data)

        plot.plot_seasonal_timeseries(data,args.plot_var,
                                      output_dir=args.out_dir)
