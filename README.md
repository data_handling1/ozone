# ozone

The aim of the project is to analyze the ozone in the atmosphere.

It provides the following shell script: `ozone`.
For usage information, type `ozone -h`.


## Prerequisites

You need a working Python environment, and `pip` installed.


## Installation
1. Create virtual environment with:

```bash
python3 -m venv env.
```

2. Activate the virtual environment with:

```bash
source env/bin/activate.
```

3. Use the following command in the base directory to install:

```bash
python -m pip install .
```

## Options and how to run it 
1. Plot altitude of variable maximum
```bash
ozone -netcdf-file <filepath> -plot-var ozone -alt-var-max-plot -out-dir <filepath>

```
2. Plot seasonal altitude of variable maximum
```bash
ozone -netcdf-file <filepath> -plot-var ozone -seasonal-plot

```
3. Plot vertical mean profiles of variable
```bash
ozone -netcdf-file <filepath> -plot-var ozone -vert-prof-plot

```
4. Plot mean of variable within altitude layer
```bash
ozone -netcdf-file <filepath> -plot-var ozone -var-layer-plot

```
5. Plot seasonal timeseries
```bash
ozone -netcdf-file <filepath> -plot-var ozone -seasonal-timeseries-plot

```
6. All together
```bash
ozone -netcdf-file <filepath> -plot-var ozone -alt-var-max-plot
-seasonal-plot -vert-prof-plot -var-layer-plot -seasonal-timeseries-plot
-out-dir <path>

```